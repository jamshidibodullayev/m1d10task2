package uz.pdp.m1d10task2.payload;

import lombok.Data;

@Data
public class GroupDto {

    private String name;
    private Long facultyId;

}
