package uz.pdp.m1d10task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.m1d10task2.entity.Faculty;

import java.util.List;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {

    boolean existsByNameAndUniversityId(String name, Long university_id);

    // Shunga teng: Select * from faculty where university_id = id;
    List<Faculty> findAllByUniversityId(Long university_id); // SHu Jpa Query hissoblanadi:

}
