package uz.pdp.m1d10task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.m1d10task2.entity.University;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {

}
