package uz.pdp.m1d10task2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M1d10task2Application {


    public static void main(String[] args) {
        SpringApplication.run(M1d10task2Application.class, args);
    }


}
